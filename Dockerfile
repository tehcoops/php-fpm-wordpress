FROM php:8.2-fpm

RUN apt update && \
    apt install -y zlib1g-dev libpng-dev libjpeg-dev libzip-dev && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-install -j$(nproc) gd mysqli zip
